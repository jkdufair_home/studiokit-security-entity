﻿using System.Data.Entity;

namespace StudioKit.Security.Entity.Interfaces
{
	/// <summary>
	/// Interface for a <see cref="DbContext"/> that includes Clients, AppClients, ServiceClients, and RefreshTokens.
	/// </summary>
	/// <typeparam name="TAppClient">Type of <see cref="AppClient"/></typeparam>
	/// <typeparam name="TServiceClient">Type of <see cref="ServiceClient"/></typeparam>
	/// <typeparam name="TRefreshToken">Type of <see cref="RefreshToken"/></typeparam>
	public interface IClientDbContext<TAppClient, TServiceClient, TRefreshToken>
		where TAppClient : AppClient
		where TServiceClient : ServiceClient
		where TRefreshToken : RefreshToken
	{
		DbSet<Client> Clients { get; set; }

		DbSet<TAppClient> AppClients { get; set; }

		DbSet<TServiceClient> ServiceClients { get; set; }

		DbSet<TRefreshToken> RefreshTokens { get; set; }
	}
}