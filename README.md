﻿# StudioKit.Security.Entity

**Note**: Requires the following peer dependencies in the Solution.
* **StudioKit.Data.Entity**
* **StudioKit.Security**

Extends the EntityFramework models from **StudioKit.Data.Entity** with models for working with OAuth.

## Interfaces

* IClientDbContext
	* Interface for a DbContext that includes Clients, AppClients, ServiceClients, and RefreshTokens.

## Models

* Client
	* Base class for other types of clients.
* AppClient
	* A client for use by an app, e.g. Web, iOS, Android. Uses `AuthorizationGrantType.AuthorizationCode` and `AuthorizationGrantType.RefreshToken`.
* ServiceClient
	* A client for use by an external service. Uses `AuthorizationGrantType.ClientCredentials`.
* RefreshToken
	* Model for storing a refresh token.
